Summary:    Utility for getting and setting Xv attributes
Name:       xvattr
Version:    1.3
Release:    46
License:    GPLv2+
URL:        https://www.dtek.chalmers.se/groups/dvd/
Source:     https://ajax.fedorapeople.org/%{name}/%{name}-%{version}.tar.gz
# Normalize documentation encoding
Patch0:     xvattr-1.3-Convert-documentation-to-UTF-8.patch
# Do not loose system CFLAGS for gxvattr
Patch1:     xvattr-1.3-Use-GTK_CFLAGS-properly.patch
# Allow to disable GTK tools
Patch2:     xvattr-1.3-Make-GTK-tools-optional.patch
BuildRequires:  autoconf
BuildRequires:  automake
BuildRequires:  coreutils
BuildRequires:  gcc
%if %{gxvattr}
BuildRequires:  gtk+-devel
%endif
BuildRequires:  libX11-devel
BuildRequires:  libXv-devel
BuildRequires:  make
BuildRequires:  perl-podlators

%description
This program is used for getting and setting Xv attributes such as
XV_BRIGHTNESS, XV_CONTRAST, XV_SATURATION, XV_HUE, XV_COLORKEY.

%package -n gxvattr
Summary: GTK1-based GUI for Xv attributes

%description -n gxvattr
GTK1-based GUI for inspecting and setting Xv attributes.

%prep
%autosetup -p1
autoreconf --install --force

%build
%configure \
%if %{gxvattr}
    --enable-gtk
%else
    --disable-gtk
%endif
%{make_build}

%install
%{make_install}

%files
%license COPYING
%doc AUTHORS ChangeLog NEWS README
%{_bindir}/xvattr
%{_mandir}/man1/*

%if %{gxvattr}
%files -n gxvattr
%license COPYING
%{_bindir}/gxvattr
%endif

%changelog
* Mon Nov 14 2022 hkgy <kaguyahatu@outlook.com> - 1.3-46
- Upgrade to v1.3-46

* Fri Sep 4 2020 lunankun <lunankun@huawei.com> - 1.3-41
- Type: bugfix
- ID: NA
- SUG: NA
- DESC: fix Source0 url

* Fri Oct 11 2019 jiangchuangang <jiangchuangang@huawei.com> - 1.3-40
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: add subpackage gxvattr

* Thu Oct 10 2019 luhuaxin <luhuaxin@huawei.com> - 1.3-39
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: move gxvattr to main package, move AUTHORS to license folder

* Fri Aug 30 2019 luhuaxin <luhuaxin@huawei.com> - 1.3-38
- Package init
